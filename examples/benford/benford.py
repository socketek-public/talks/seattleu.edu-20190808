from math import log10
from pprint import pprint


GBT = {}


def benford_compact(d, n):
    if d == 0 and n == 1:
        return None

    return round(
        sum(
            log10(1 + (1.0 / (10 * k + d))) for k in range(
                int(10 ** (n - 2)), int(10 ** (n-1))
            )
        ),
        5
    )


def benford(d, n):
    if d == 0 and n == 1:
        return None

    start = 10 ** (n - 2)
    start = int(start)

    end = 10 ** (n - 1)
    end = int(end)

    probabilities = []
    for k in range(start, end):
        probability = log10(1 + (1 / (10 * k + d)))
        probabilities.append(probability)

    total = sum(probabilities)

    return round(total, 5)


# note: d is 0-based, n is 1-based.
for d in range(0, 10):
    GBT[d] = {}
    for n in range(1, 6):
        GBT[d][n] = benford(d, n)


print('-----------------------------------------------------------------')
print(' Generalized Benford Table')
print('-----------------------------------------------------------------')
pprint(GBT)
print('=================================================================')


print(f' P(0, 1) = {GBT[0][1]}')
print(f' P(2, 2) = {GBT[2][2]}')
print(f' P(3, 3) = {GBT[3][3]}')
print(f' P(9, 5) = {GBT[9][5]}')

print(f'bc(9, 5) = {benford_compact(9, 5)}')
