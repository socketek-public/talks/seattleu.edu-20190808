from math import log10
from pprint import pprint


GBT = {}


def P(d, n):
    if d == 0 and n == 1:
        return None

    start = 10 ** (n - 2)
    start = int(start)

    end = 10 ** (n - 1)
    end = int(end)

    probabilities = []
    for k in range(start, end):
        probability = log10(1 + (1 / (10 * k + d)))
        probabilities.append(probability)

    total = sum(probabilities)

    return round(total, 5)


# note: d is 0-based, n is 1-based.
for d in range(0, 10):
    GBT[d] = {}
    for n in range(1, 6):
        GBT[d][n] = P(d, n)


pprint(GBT)
