from math import log10


def P(d, n):
    start = 10 ** (n - 2)
    end = 10 ** (n - 1)

    probabilities = []
    for k in range(start, end):
        probability = log10(1 + (1 / (10 * k + d)))
        probabilities.append(probability)

    total = sum(probabilities)

    return round(total, 5)


d = 0; n = 2  # note: d is 0-based, n is 1-based.
print(f'd={d}, n={n}, P={P(d, n)}')
