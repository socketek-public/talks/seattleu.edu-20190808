from math import log10


# note: d is 0-based, k is 0-based.
d = 1; k = 0
probability = log10(1 + (1 / ((10 * k) + d)))
print(probability)

d = 2; k = 0
probability = log10(1 + (1 / ((10 * k) + d)))
print(probability)
