# Recorded with the doitlive recorder
#doitlive shell: /bin/bash
#doitlive prompt: default

```python
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

tips = sns.load_dataset('tips')

tips.shape
tips.head()

tips.iloc[0, :]
tips.iloc[:, 0].head()
tips['total_bill'].head()

# Normalize bill across diners
tips['ibill'] = tips['total_bill'] / tips['size']

# Normalize tips across diners
tips['itip'] = tips['tip'] / tips['size']

# Normalize tips
tips['ntip'] = tips['itip'] / tips['ibill']

sns.distplot(tips['ntip'])
tips['ntip'].describe()
plt.show()
plt.clf()

sns.boxplot(x='size', y='ntip', hue='time', data=tips, palette='Set3')
plt.show()
```
