import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns

tips = sns.load_dataset('tips')

print(tips.shape)
print(tips.head())

print(tips.iloc[0, :])
print(tips.iloc[:, 0].head())
print(tips['total_bill'].head())

# Normalize bill across diners
tips['ibill'] = tips['total_bill'] / tips['size']

# Normalize tips across diners
tips['itip'] = tips['tip'] / tips['size']

# Normalize tips
tips['ntip'] = tips['itip'] / tips['ibill']

sns.distplot(tips['ntip'])
print(tips['ntip'].describe())
plt.show()
plt.clf()

sns.boxplot(x='size', y='ntip', hue='time', data=tips, palette='Set3')
plt.show()
