import pandas as pd

df = pd.read_csv('simpsons.csv')

print(df.head())

# Stats
print(df['age'].sum())
print(df['age'].count())
print(df['age'].mean())

# Index
print(df.iloc[2]['age'])
print(df.iloc[2]['score'])

# Sort
print(df.sort_values('score'))
print(df.sort_values('score', ascending=False))
print(df.sort_values(['score', 'age'], ascending=False))

# Filter
print(df['name'])
print(df[df['name'] == 'maggie'])

print(df['age'].min())
print(df['age'].max())

median_age = df['age'].median()
print(df[df['age'] > median_age])

# Compound filter
cond = (df['name'] == 'homer') & (df['score'] > 30)
print(df[cond])

cond = (df['name'] == 'homer') & (df['score'] > 32)
print(df[cond])

# Choose random
print(df.sample(1))
print(df.sample(2))
print(df.sample(2))
