```python
import pandas as pd

df = pd.read_csv('simpsons.csv')

df.head()

# Stats
df['age'].sum()
df['age'].count()
df['age'].mean()

# Index
df.iloc[2]['age']
df.iloc[2]['score']

# Sort
df.sort_values('score')
df.sort_values('score', ascending=False)
df.sort_values(['score', 'age'], ascending=False)

# Filter
df['name']
df[df['name'] == 'maggie']

df['age'].min()
df['age'].max()

median_age = df['age'].median()
df[df['age'] > median_age]

# Compound filter
cond = (df['name'] == 'homer') & (df['score'] > 30)
df[cond]

cond = (df['name'] == 'homer') & (df['score'] > 32)
df[cond]

# Choose random
df.sample(1)
df.sample(2)
df.sample(2)
```
