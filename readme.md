# Talk #

```
org: seattleu.edu
date: 2019.08.08
tags: #python
```

## Links ##

[Benford Distribution](http://www.statisticalconsultants.co.nz/blog/benfords-law-and-accounting-fraud-detection.html)

## Resources ##

### Beginner ###

#### Software ####

[Python Installer](https://www.python.org/downloads/)

#### Tutorials ####

[Learn Python The Hard Way](https://github.com/ubarredo/LearnPythonTheHardWay)

#### Books ####

[The Hitchhiker's Guide to Python](https://docs.python-guide.org/)

[Real Python](https://realpython.com/)

[Automate The Boring Stuff](https://automatetheboringstuff.com/)

***

### Intermediate ###

#### Software ####

#### Tutorials ####

[Pandas](https://pandas.pydata.org/pandas-docs/stable/getting_started/tutorials.html)

[Seaborn](https://seaborn.pydata.org/tutorial.html)

[IPython](https://ipython.readthedocs.io/en/stable/interactive/tutorial.html)

#### Books ####

[Think Stats](https://greenteapress.com/wp/think-stats-2e/)

[A Layered Grammar of Graphics](http://vita.had.co.nz/papers/layered-grammar.pdf)
